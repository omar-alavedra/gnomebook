const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {  
    devtool: 'inline-source-map',
    entry: './src/index.js',  
    output: {    
        path: path.resolve(__dirname, 'build'),
        publicPath: '/',
        filename: 'bundle.js'
    },  
    devServer: {    
        contentBase: './build',  
        historyApiFallback: true,
    },  
    module: {    
        rules: [
            {      
                test: /\.(js|jsx)$/,      
                exclude: /node_modules/,      
                use: ['babel-loader']     
            },
            {
                test: /\.(scss|sass)$/,
                exclude: /node_modules/,
                loaders: [
                  MiniCssExtractPlugin.loader,
                  {
                    loader: 'css-loader',
                    options: {
                        sourceMap: true
                    }
                  },
                  {
                      loader: 'sass-loader',
                      options: {
                          sourceMap: true
                      }
                  }

                ]
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.(eot|woff|woff2|ttf|svg|otf)$/,
                loaders: [
                  'url-loader'
                ]
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                use: [
                  {
                    loader: 'file-loader',
                  },
                ],
            },
        ],
        
    },
    plugins: [
        new HtmlWebpackPlugin({
          template: path.resolve('./index.html'),
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
        })
    ]
};
