# PROJECT GNOMEBOOK
by Omar Alavedra.

## Brief introduction
This project is a little project based on a frontend code assessment. 
Is made with Node.js and React.js and the reason why is because of the great efficiency, the high performance, 
modularity and also scalable for the future.
The main duty of this project called GnomeBook:
    - Fetch and display data from a specific URL, like if it was a fetch from a server.
    - Show more/less components
    - Filter components/elements through name, and other filters.
    - Display all the basic information of the components.
    - Be efficient loading and filtering the elements.
I hope the you enjoy interacting with Gnomebook, I accept all kinds of suggestions for improvement. Thank you very much for the opportunity.

## Improvements for the future
Based on the time of the development (One and half days), there are some improvements to do in the near future like:
    - Improve the search algorithm.
    - Improve the filters of the search for all the use cases.
    - Improve layout for specific devices and screen sizes.
    - Other bugs or improvements to be tested with time and solved.


## Requirements

For development, you will need Node.js installed on your environement.

#### Node installation on OS X

You will need to use a Terminal. On OS X, you can find the default terminal in
`/Applications/Utilities/Terminal.app`.

Please install [Homebrew](http://brew.sh/) if it's not already done with the following command.

    $ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

If everything when fine, you should run

    brew install node

#### Node installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs

#### Node installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it.

---

## Install

    $ git clone https://gitlab.com/omar-alavedra/gnomebook.git
    $ cd gnomebook
    $ npm install

## Start & watch (development)

    $ npm run start

## Simple build for production

    $ npm run build:prod
    
## Test & watch

    $ npm test -- --watch
    
## Languages & tools

### JavaScript

- [Webpack](https://webpack.js.org) is a module builder used in the development to load elements like the HTML, CSS files for example.
- [Redux](https://redux.js.org) is a general storehouse for containing a general state to work with in React.
- [React Router](https://reacttraining.com/react-router/) is used for routing in React.
- [Babel](https://babeljs.io) is a toolchain that is mainly used to convert ECMAScript 2015+ code into a backwards compatible version of JavaScript.
- [es6 syntax](http://es6.github.io/) is used for an enriched Javascript syntax [es6ify](https://github.com/thlorenz/es6ify).
- [React](http://facebook.github.io/react) is used for UI.

### CSS

- [sass](https://sass-lang.com) is an extension of CSS that enables you to use things like variables, nested rules, inline imports and more.


### Testing tools for Javascript

- [jest](https://sass-lang.com) is a javascript testing framework (basic).
- [enzyme](https://jestjs.io) is a JavaScript Testing utility for React that makes it easier to test the React Components' output.


