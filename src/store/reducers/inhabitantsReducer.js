import { SET_INHABITANTS, FILTER_INHABITANTS, SET_INHABITANTS_PROFESSIONS } from '../actions/actionTypes';

/* inhabitantsReducer initial redux state */
const initState = {
    inhabitants: [],
    filteredInhabitants: [],
    fetched: false,
    inhabitantsProfessions: []
}

/*
     * InhabitantsReducer
     * @param {Array} state - The redux actual state of the inhabitatns reducer.
     * @param {Bool} action - Indicates the action parameters and type.
*/
const inhabitantsReducer= (state = initState, action)=>{
    switch (action.type) {
        case SET_INHABITANTS:
            return {
                ...state,
                inhabitants: action.inhabitants,
                filteredInhabitants: action.filteredInhabitants,
                fetched: action.fetched
            }
        case FILTER_INHABITANTS:
            return {
                ...state,
                filteredInhabitants: action.filteredInhabitants,
            }
        case SET_INHABITANTS_PROFESSIONS:
        return {
            ...state,
            inhabitantsProfessions: action.inhabitantsProfessions,            
        }
        default:
            return state;
    }

}
export default inhabitantsReducer;