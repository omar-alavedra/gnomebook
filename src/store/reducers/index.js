import { combineReducers } from 'redux';
import inhabitantsReducer from './inhabitantsReducer';

const rootReducer = combineReducers({
    inhabitantsReducer: inhabitantsReducer,
});

export default rootReducer;