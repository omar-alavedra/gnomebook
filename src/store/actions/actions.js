import { 
    SET_INHABITANTS,
    SET_INHABITANTS_PROFESSIONS,
    FILTER_INHABITANTS,

} from  './actionTypes';

/*
     * Set inhabitants
     * @param {Array} inhabitants - The list of inhabitants to save on redux state.
     * @param {Bool} fetched - Indicates if the fetch from the server has been done.
*/
export const setInhabitants = (inhabitants, fetched) => {
    // console.log(inhabitants)
    return {
        type: SET_INHABITANTS,
        inhabitants: inhabitants,
        filteredInhabitants: inhabitants,
        fetched: fetched
    }
}

/*
     * Set professions
     * @param {Array} inhabitantsProfessions - The list of different professions
*/
export const setProfessions = (inhabitantsProfessions) => {
    // console.log(filteredInhabitants)
    return {
        type: SET_INHABITANTS_PROFESSIONS,
        inhabitantsProfessions: inhabitantsProfessions,
    }
}

/*
     * FilterInhabitants
     * @param {Array} filteredInhabitants - The list of inhabitants (filtered) to save on redux state.
*/
export const filterInhabitants = (filteredInhabitants) => {
    // console.log(filteredInhabitants)
    return {
        type: FILTER_INHABITANTS,
        filteredInhabitants: filteredInhabitants,
    }
}
