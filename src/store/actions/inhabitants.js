import { 
    setInhabitants,
    setProfessions
} from "./actions";

/*
     * GetInhabitants Fetches from the URL (server) the inhabitants from the specified town in the parameter.
     * @param {String} town - Indicates the town of inhabitants to be fetched.
*/
export const getInhabitants = (town) => {                

    return dispatch => {
        fetch('https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json',{
        })
        .then(res => res.json())
        .then(parsedRes => {
            if (parsedRes){
                dispatch(setInhabitants(parsedRes,true));

                let currentList = parsedRes[town.toString()];
                var professions = new Set([]);
                //--We filter the professions of the inhabitants--
                currentList.filter(inhabitant => {
                    inhabitant.professions.map(profession =>{
                        professions.add(profession.trim())
                    });
                });

                //--Setup the sorted profession as an Array to be displayed in the element Select required format.
                const sortedProfessions = [...professions].sort();
                var finalProfessions = [
                    { 
                        value: "select", 
                        label: "Professions..." , 
                        id:"profession"
                    }
                ]
                sortedProfessions.map(profession =>{
                    const element = { 
                        value: profession.toLowerCase(), 
                        label: profession , 
                        id:"profession"
                    }

                    finalProfessions.push(element)

                });

                dispatch(setProfessions(finalProfessions))
            }
        })
        .catch(err =>{
            console.log("Error fetching the inhabitants: ", err)
        })
        
    }
}