//-- React tools
import React from 'react';
import { Link } from "react-router-dom";

//3rd party components
import { animateScroll as scroll} from "react-scroll";

/** Class representing the Footer for desktop component. */
export default class FooterMobile extends React.Component {
    /*
     * Create Footer.
        * @param scroll - To controll the scroll interaction
    */
    constructor(props) {
        super(props);
        this.state = {
            scroll: false
        }

    }

    /*
        * hadleScroll - scrolls the view of the screen to the position (after clicking the arrow)
    */
    handleScrollEvent = () => {
        if(this.state.scroll){
            scroll.scrollToTop();
            this.setState({
                scroll: !this.state.scroll
            })
        }
        else{
            scroll.scrollToBottom();
            this.setState({
                scroll: !this.state.scroll
            })
        }
    }

    /** Render FooterMobile*/
    render() {
        return(
            <div className="footerMobile">
                <div className="arrow">
                    <button onClick={this.handleScrollEvent}>
                        <img src={require('./images/icons/ic_arrow.png')}/>
                    </button>                
                </div>
                <nav className="footeNavigation">
                   
                    <div className="footerTitle">
                        <p>GnomeBook</p>
                    </div>
                    <div className="footerContent">
                            <div className="footerContentColumn1">
                                <div className="footerContentItem">
                                    <a>Conócenos</a>
                                    <ul>
                                        <li><Link to="/" target="_blank">¿What is GnomeBook ?</Link></li>
                                    </ul>
                                </div>
                                <div className="footerContentItem">
                                    <a>Legal</a>
                                    <ul>
                                        <li> <a href={"mailto:gnomeBook@gnome.com"}>Contact us </a></li>
                                        <li> <Link to="/" target="_blank">Our Gnome privacy</Link> </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="footerContentColumn2">
                                <div className="footerContentItem">
                                    <a>Follow Us</a>
                                    <ul>
                                        <li> <a href={"mailto:gnomeBook@gnome.com"}>GnomeBook </a></li>
                                        <li> <a href={"mailto:gnomeBook@gnome.com"}>GnomeGram </a></li>
                                        <li> <a href={"mailto:gnomeBook@gnome.com"}>YouGnome </a></li>
                                    </ul>
                                </div>
                                
                            </div>
                    </div>
                    <div className="footerBottom">
                        <div className="footerSpacer"/>
                        <div className="copyrightMobile">
                            <a>© 2019 Gnomebook</a>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
};
