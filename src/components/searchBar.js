//-- React tools
import React from 'react';
import { connect } from "react-redux";
import Select from 'react-select';

//--3rd PARTY COMPONENTS
import { isBrowser, isMobileOnly} from "react-device-detect";

//--COMPONENTS
import {filterInhabitants } from '../store/actions/index';

/** Class representing the SearchBar for desktop component. */
class SearchBar extends React.Component {
    /*
     * Create Home.
     * @param {Array} colors - The different color options to filter
     * @param {string} town - The townName for the initial display
    */
    constructor(props) {
        super(props);
        this.state = {
            colors: [
                { value: 'select', label: 'Hair color...' , id:"hairColor"},
                { value: 'red', label: 'Red' , id:"hairColor"},
                { value: 'green', label: 'Green' , id:"hairColor"},
                { value: 'black', label: 'Black' , id:"hairColor"},
                { value: 'pink', label: 'Pink' , id:"hairColor"},
                { value: 'gray', label: 'Gray' , id:"hairColor"},
            ],
            townName: this.props.townName
        }    
        this.handleInput = this.handleInput.bind(this);
    };
    
    /*
        * Handle the different inputs (from searchbar, colorHair filter or profession filter), also updates the filtered array on Redux state.
        * @param {string} color1 - The first color, in hexadecimal format.
        * @param {string} color2 - The second color, in hexadecimal format.
        * @return {string} The blended color.
    */
    handleInput(e) {
        // e.preventDefault();
        if(e.target && e.target.id === "nameSearch"){
            //Load of the current list and initialization of variables for the new filtered elements
            let currentList = this.props.filteredInhabitants;
            let filter = [];
            let filteredInhabitants = [];

            if (e.target.value !== "") {
                //Filter the elements from the current list, matching the input value from the user
                filter = currentList[this.state.townName].filter(inhabitant => {
                    const lc = inhabitant.name.toLowerCase();
                    const filter = e.target.value.toLowerCase();
                    return lc.includes(filter);
                });
                filteredInhabitants[this.state.townName] = filter;
            } 
            else if(e.target.value === ""){
                //Reset when the input value is empty
                filteredInhabitants = this.props.inhabitants;
            }
            else {
                //Reset when the input value is empty
                filteredInhabitants = this.props.filteredInhabitants;
            }
            this.props.filterInhabitants(filteredInhabitants)
        }
        else if(e.id === "hairColor"){

            let currentList = this.props.filteredInhabitants;
            let filter = [];
            let filteredInhabitants = [];
            if (e.value !== "" && e.value !== "select") {
                //Filter the elements from the current list, matching the selected color value from the user
                filter = currentList[this.state.townName].filter(inhabitant => {
                    const lc = inhabitant.hair_color.toLowerCase();
                    const filter = e.value.toLowerCase();
                    return lc.includes(filter);
                });
                filteredInhabitants[this.state.townName] = filter;
            } else  {
                    filteredInhabitants = this.props.inhabitants;
            }
            this.props.filterInhabitants(filteredInhabitants)
        }
        else if(e.id === "profession"){
            let currentList = this.props.filteredInhabitants;
            let filter = [];
            let filteredInhabitants = [];
            if (e.value !== "" && e.value !== "select") {
                //Filter the elements from the current list, matching the selected profession value from the user
                filter = currentList[this.state.townName].filter(inhabitant => {
                    const lc = inhabitant.professions;
                    const filter = e.label;
                    return lc.includes(filter);
                });
                filteredInhabitants[this.state.townName] = filter;
            } else  {
                    filteredInhabitants = this.props.inhabitants;
            }
                this.props.filterInhabitants(filteredInhabitants)
        }

    }
       
    /** Render the searchBar based on the device*/
    render() {
        if(isBrowser){
            return(
                <div className="searchBar">
                    <input type="text" id="nameSearch" className="input" placeholder="Search..." onChange={this.handleInput}/>   
                    <Select id="hairColorSelector" menuPlacement="bottom" placeholder="Hair color..." onChange={this.handleInput} options={this.state.colors} className="selectorHair"/>
                    <Select id="hairColorSelector" menuPlacement="bottom" placeholder="Professions..." onChange={this.handleInput} options={this.props.inhabitantsProfessions} className="selectorHair"/>
                </div>
            );
        }
        else if(isMobileOnly){
            return(
                <div className="searchBarMobile">
                    <input type="text" id="nameSearch" className="input" placeholder="Search..." onChange={this.handleInput}/>   
                    <div className="selectors">
                        <Select id="hairColorSelector" menuPlacement="bottom" placeholder="Hair color..." onChange={this.handleInput} options={this.state.colors} className="selector"/>
                        <Select id="hairColorSelector" menuPlacement="bottom" placeholder="Professions..." onChange={this.handleInput} options={this.props.inhabitantsProfessions} className="selector"/>
                    </div>
                    
                </div>
            );
        }
       
    }
};

/** Map the general state from Redux to props to be used by the SearchBar.*/
const mapStateToProps = (state)=>{
    return {
        inhabitants: state.inhabitantsReducer.inhabitants,
        fetched: state.inhabitantsReducer.fetched,
        filteredInhabitants: state.inhabitantsReducer.filteredInhabitants,
        inhabitantsProfessions: state.inhabitantsReducer.inhabitantsProfessions
    }
}

/** Dispatch the Redux functions to props to be used by the searchBar.*/
const mapDispatchToProps= (dispatch)=>{
    return{
        filterInhabitants: (filteredInhabitants) => dispatch(filterInhabitants(filteredInhabitants)),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(SearchBar);