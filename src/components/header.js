//-- React tools
import React from 'react';
import { Link } from 'react-router-dom';

//-- COMPONENTS
import SearchBar from '../components/searchBar';

/** Class representing the Header for desktop component. */
export default class Header extends React.Component {
    /*
     * Create Header.
    */
    constructor(props) {
        super(props);  
    };

    /** Render Header*/
    render() {
        return(
            <div>
                <header className={"headerScroll"} style={{ boxShadow: 'no'}}>
                    <nav className="header__navigation">
                        <div className="header__navlinks">
                            <nav className="links__wrapper">
                                <div className="link1" >
                                    <Link to="/">GnomeBook</Link>
                                </div>
                                <div className="link__separator">
                                    |
                                </div>
                                <div className="link3">
                                    <Link to="/">Brastlewark </Link>
                                </div>
                            </nav>
                        </div>
                        <div>
                            <div className="searchBar__wrapper">
                                <SearchBar townName={"Brastlewark"}/>
                            </div>
                        </div>
                    </nav>
                </header>
            </div>
        );
    }
};
