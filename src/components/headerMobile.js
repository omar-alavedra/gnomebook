//-- React tools
import React from 'react';
import { Link } from 'react-router-dom';

//-- COMPONENTS
import SearchBar from '../components/searchBar';

/** Class representing the HeaderMobile for mobile component. */
export default class HeaderMobile extends React.Component {
    /*
     * Create Header.
    */
    constructor(props) {
        super(props);  
    };
   
    /** Render Header*/
    render() {
        return(
            <div className="headerMobileBase">
                <header className={"headerScrollMobile"} style={{ boxShadow: 'no'}}>
                    <nav className="headerMobileNavigation">
                        <div className="logoMobile__wrapper">
                            <div className="link1" >
                                <Link to="/">GnomeBook</Link>
                            </div>
                            <div className="linkSeparator">
                                |
                            </div>
                            <div className="link2">
                                <Link to="/">Brastlewark </Link>
                            </div>
                        </div>
                        <div className="searchBarWrapper">
                            <SearchBar townName={"Brastlewark"}/>
                        </div>
                    </nav>
                </header>
            </div>
        );
    }
};