//-- React tools
import React from 'react';
import { Route, Link, BrowserRouter as Router, Switch } from 'react-router-dom';
import Home from '../pages/home';
import NotFound from '../pages/notFound';
import { Provider } from 'react-redux';
import { store } from '../store/configureStore';

/**
     * Route hierarchy of the user's navigation.
*/
const Routes = (match) => {
    return(
        <Router>
            <div>
                <Provider store={store}>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route component={NotFound} />
                    </Switch>
                </Provider>
            </div>
        </Router>
    )
}

export default Routes;
