import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import HeaderMobile from '../../components/header';

test('should render HeaderMobile correctly', ()=>{
    const renderer = new ReactShallowRenderer;
    renderer.render(<HeaderMobile/>);
    expect(renderer.getRenderOutput()).toMatchSnapshot()
})