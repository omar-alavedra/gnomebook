import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import FooterMobile from '../../components/header';

test('should render FooterMobile correctly', ()=>{
    const renderer = new ReactShallowRenderer;
    renderer.render(<FooterMobile/>);
    expect(renderer.getRenderOutput()).toMatchSnapshot()
})