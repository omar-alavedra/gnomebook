import React from 'react';
import { 
    loadElements
} from "../../../src/services/index";
import toJSON from 'enzyme-to-json';
import {shallow} from 'enzyme';

test('loadElements should return the rendered elements (inhabitants) passed through props', ()=>{
    const inhabitantsTestList = [
        {
            "id":2,
            "name":"Malbin Chromerocket",
            "thumbnail":"http://www.publicdomainpictures.net/pictures/30000/nahled/maple-leaves-background.jpg",
            "age":166,
            "weight":35.88665,
            "height":106.14395,
            "hair_color":"Red",
            "professions":["Cook","Baker","Miner"],
            "friends":["Fizwood Voidtossle"]
        }
    ]
    const action = shallow(<loadElements elements={inhabitantsTestList}/>);
    expect(toJSON(action)).toMatchSnapshot()
})

test('loadElements should return an empty list message', ()=>{
    const action = shallow(<loadElements elements={[]}/>);
    expect(toJSON(action)).toMatchSnapshot()
})