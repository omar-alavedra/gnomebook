import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

//--configuration needed to adapt enzyme working with react 16
Enzyme.configure({
    adapter: new Adapter()
})