import inhabitantsReducer from '../../../store/reducers/inhabitantsReducer'

test('should test default values', ()=>{
    const state = inhabitantsReducer(undefined,{ type: '@@INIT' });
    expect(state).toEqual({
        inhabitants: [],
        filteredInhabitants: [],
        fetched: false,
        inhabitantsProfessions: []
    })
})

test('should set inhabitants to the array of inhabitants obtained from the fetch', ()=>{
    const currentState = {
        inhabitants: [],
        filteredInhabitants: [],
        fetched: false,
        inhabitantsProfessions: []
    }
    const inhabitantsTestList = [
        {
            "id":2,
            "name":"Malbin Chromerocket",
            "thumbnail":"http://www.publicdomainpictures.net/pictures/30000/nahled/maple-leaves-background.jpg",
            "age":166,
            "weight":35.88665,
            "height":106.14395,
            "hair_color":"Red",
            "professions":["Cook","Baker","Miner"],
            "friends":["Fizwood Voidtossle"]
        }
    ]
    const action = {
        type: 'SET_INHABITANTS',
        inhabitants: inhabitantsTestList,
        filteredInhabitants: inhabitantsTestList,
        fetched: true
    }
    const state = inhabitantsReducer(currentState,action);
    
    expect(state).toEqual({
        inhabitants: action.inhabitants,
        filteredInhabitants: action.filteredInhabitants,
        fetched: true,
        inhabitantsProfessions: []
    })
})

test('should set filteredInhabitants to the array of filtered inhabitants obtained from the external function', ()=>{
    const inhabitantsTestList = [
        {
            "id":2,
            "name":"Malbin Chromerocket",
            "thumbnail":"http://www.publicdomainpictures.net/pictures/30000/nahled/maple-leaves-background.jpg",
            "age":166,
            "weight":35.88665,
            "height":106.14395,
            "hair_color":"Red",
            "professions":["Cook","Baker","Miner"],
            "friends":["Fizwood Voidtossle"]
        }
    ]
    const action = {
        type: 'FILTER_INHABITANTS',
        filteredInhabitants: inhabitantsTestList,
    }
    const state = inhabitantsReducer(undefined,action);
    
    expect(state.filteredInhabitants).toBe(action.filteredInhabitants)
})
// const initState = {
//     inhabitants: [],
//     filteredInhabitants: [],
//     fetched: false,
//     inhabitantsProfessions: []
// }
// const inhabitantsReducer= (state = initState, action)=>{
//     // console.log(action)
//     switch (action.type) {
//         case SET_INHABITANTS:
//             return {
//                 ...state,
//                 inhabitants: action.inhabitants,
//                 filteredInhabitants: action.filteredInhabitants,
//                 fetched: action.fetched
//             }
//         case FILTER_INHABITANTS:
//             return {
//                 ...state,
//                 filteredInhabitants: action.filteredInhabitants,
//             }
//         case SET_INHABITANTS_PROFESSIONS:
//         return {
//             ...state,
//             inhabitantsProfessions: action.inhabitantsProfessions,            
//         }
//         default:
//             return state;
//     }

// }
// export default inhabitantsReducer;