import { 
    setInhabitants,
    setProfessions,
    filterInhabitants
} from "../../../store/actions/actions";

test('should setup Inhabitants list into the general state', ()=>{
    const inhabitantsTestList = [
        {
            "id":2,
            "name":"Malbin Chromerocket",
            "thumbnail":"http://www.publicdomainpictures.net/pictures/30000/nahled/maple-leaves-background.jpg",
            "age":166,
            "weight":35.88665,
            "height":106.14395,
            "hair_color":"Red",
            "professions":["Cook","Baker","Miner"],
            "friends":["Fizwood Voidtossle"]
        }
    ]
    const action = setInhabitants(inhabitantsTestList,true);
    expect(action).toEqual({
        type: 'SET_INHABITANTS',
        inhabitants: inhabitantsTestList,
        filteredInhabitants: inhabitantsTestList,
        fetched: true
    })
})

test('should setup Inhabitants professions into the general state', ()=>{
    const professionsTestList = [ "Cook", "Baker", "Miner"]
    const action = setProfessions(professionsTestList);
    expect(action).toEqual({
        type: 'SET_INHABITANTS_PROFESSIONS',
        inhabitantsProfessions: professionsTestList,
    })
})

test('should setup Inhabitants filtered list into the general state', ()=>{
    const inhabitantsTestList = [
        {
            "id":2,
            "name":"Malbin Chromerocket",
            "thumbnail":"http://www.publicdomainpictures.net/pictures/30000/nahled/maple-leaves-background.jpg",
            "age":166,
            "weight":35.88665,
            "height":106.14395,
            "hair_color":"Red",
            "professions":["Cook","Baker","Miner"],
            "friends":["Fizwood Voidtossle"]
        }
    ]
    const action = filterInhabitants(inhabitantsTestList);
    expect(action).toEqual({
        type: 'FILTER_INHABITANTS',
        filteredInhabitants: inhabitantsTestList,
    })
})