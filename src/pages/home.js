//-- React tools
import React from 'react'
import '../styles/styles.scss';
import { connect } from "react-redux";

//-- COMPONENTS -- desktop - mobile
import Header from '../components/header';
import HeaderMobile from '../components/headerMobile';
import Footer from '../components/footer';
import FooterMobile from '../components/footerMobile';
import { LoadElements } from '../services/index';
import {getInhabitants } from '../store/actions/index';

//--3rd party components
import { isBrowser, isMobileOnly} from "react-device-detect";

/** Class representing the HomePage component. */

class Home extends React.Component {
    /*
     * Create Home.
     * @param town - The townName for the initial display
    */
    constructor(props) {
        super(props);
        this.state = {
            town: "Brastlewark",
            numberDisplay: 21,
        };
    }

    /** Load on REDUX the inhabitants of the town specifiend previously*/
    componentWillMount = () => {
        this.props.getInhabitants(this.state.town)
    }

    /*
        * UpdateList - updates the value of elements to be displayed.
    */
    updateList = (e) =>{

        const id = e.target.id;
        var number = this.state.numberDisplay;

        if(id ==="more"){
            if(number < this.props.filteredInhabitants[this.state.town].length - 22){
                number += 21;
            }
            else{
                //Not able to display more
            }
        }
        else if(id=="less"){
            if(number - 21 >= 21){
                number -= 21;
            }
            else{
                //Not able to display less than the minimum (21)
            }
        }
        this.setState({
            ...this.state,
            numberDisplay: number
        })

    }

    /** Render Home Desktop or Mobile version depending on the device*/
    render() {
    if(isBrowser){
        return (
            <div className="home">
                <Header/>
                <div className="homeHeader" >
                    <div className="elementsWrapper">
                        <div className="elements">
                            {this.props.fetched?<LoadElements elements = {this.props.filteredInhabitants[this.state.town]} numberDisplay={this.state.numberDisplay}/>:<div>There was an error loading the users!</div>}
                            {this.props.fetched?<div className="loadMoreLess">
                                <button id="more" onClick={this.updateList}>Show more</button>
                                {this.state.numberDisplay>21?<button id="less" onClick={this.updateList}>Show less</button>:null}
                            </div>:null}
                        </div>
                        
                    </div>
                </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                <Footer/>
            </div>
        )
    }
    if(isMobileOnly) {
        return (
            <div className="homeScreenMobile">
                <HeaderMobile/>
                    <div className="homeMobileHeader" >
                        <div className="elementsWrapper" >
                            <div className="elements">
                                {this.props.fetched?<LoadElements elements = {this.props.filteredInhabitants[this.state.town]} numberDisplay={this.state.numberDisplay}/>:<div>There was an error loading the users!</div>}
                                {this.props.fetched?<div className="loadMoreLess">
                                    <button id="more" onClick={this.updateList}>Show more</button>
                                    {this.state.numberDisplay>21?<button id="less" onClick={this.updateList}>Show less</button>:null}
                                </div>:null}
                            </div>
                        </div>
                    </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                <FooterMobile/>
            </div>
        )
    }
  }
}

/** Map the general state from Redux to props to be used by Home.*/
const mapStateToProps = (state)=>{
    return {
        inhabitants: state.inhabitantsReducer.inhabitants,
        fetched: state.inhabitantsReducer.fetched,
        filteredInhabitants: state.inhabitantsReducer.filteredInhabitants
    }
}

/** Dispatch the Redux functions to props to be used by Home.*/
const mapDispatchToProps= (dispatch)=>{
    return{
        getInhabitants: (town) => dispatch(getInhabitants(town)),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Home);