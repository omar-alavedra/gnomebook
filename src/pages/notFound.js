//-- React tools
import React from 'react';
import { Redirect } from 'react-router';

/** Class representing the NotFound component. */
const NotFound = () => {
    return(
        <div>
            <Redirect push to="/" />
        </div>
    )
}

export default NotFound;