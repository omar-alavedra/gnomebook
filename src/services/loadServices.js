//-- React tools
import React from "react";

//--3rd PARTY COMPONENTS
import { isBrowser, isMobileOnly} from "react-device-detect"; 
import Fade from 'react-reveal/Fade';

/** Class representing the list of elements to display. */
export class LoadElements extends React.Component {

    /** Render LoadElements*/
    render(){
      if(isBrowser){
        if(this.props.elements && this.props.elements.length>0)
        {
            return(
                this.props.elements.map((inhabitant,index) => {
                    var image = ''
                    if(inhabitant.thumbnail) image = inhabitant.thumbnail
                    if(index < this.props.numberDisplay){
                        return ( 
                            <Fade key={index}>                        
                            <div className="infoCard">
                                <div className="physicalInfo">
                                    <div className="infoContainer">
                                        <div className="infoElement">
                                            <a>Name:</a> <a id="bold">{inhabitant.name}</a>
                                        </div>
                                        <div className="infoElement">
                                            <a>Age: </a> <a id="bold">{inhabitant.age}</a>
                                        </div>
                                        <div className="infoElement">
                                            <a>Hair Color: </a> <a id="bold"> {inhabitant.hair_color}</a>
                                        </div>
                                        <div className="infoElement">
                                            <a>Height: </a> <a id="bold"> {(parseFloat(inhabitant.height)).toFixed(2).toString().replace(".",",")}</a>
                                        </div>
                                        <div className="infoElement">
                                            <a>Width: </a> <a id="bold"> {(parseFloat(inhabitant.weight)).toFixed(2).toString().replace(".",",")}</a>
                                        </div>
                                    </div>
                                    <div className="imageContainer">
                                        <img src={image}/>
                                    </div>
                                </div>
                                <div className="additionalInfo">
                                    {inhabitant.friends.length>0?
                                    <div className="socialInfo">
                                        <a>Friends: </a> 
                                        <div className="socialInfoContent">
                                            {inhabitant.friends.map((friend,index) => {
                                                if(index < inhabitant.friends.length-1){
                                                    return (
                                                        <a key={index}> {friend}, </a>
                                                    )
                                                }
                                                else{
                                                    return (
                                                        <a key={index}> {friend} </a>
                                                    )
                                                }
                                                
                                            })}
                                        </div>
                                    </div>:null}
                                    {inhabitant.professions.length>0?
                                    <div className="professionalInfo">
                                        <a>Professions: </a>   
                                        <div className="professionalInfoContent">                             
                                            {inhabitant.professions.map((profession,index) => {
                                                if(index < inhabitant.professions.length-1){
                                                    return (
                                                        <a key={index}> {profession}, </a>
                                                    )
                                                }
                                                else{
                                                    return (
                                                        <a key={index}> {profession} </a>
                                                    )
                                                }
                                            })}
                                        </div>
                                    </div>:null
                                    }
                                    
                                    
                                </div>
                            </div>
                            </Fade>
                        )
                    }
                    
                })
            );
        }
        else{
            return (
                <div className="noInhabitants">
                    There are no Gnomes around! Join us to be the first!
                </div>
            );
        }
      }
      else if(isMobileOnly){
        if(this.props.elements && this.props.elements.length>0)
        {

            return(
                this.props.elements.map((inhabitant,index) => {
                    var image = ''
                    if(inhabitant.thumbnail) image = inhabitant.thumbnail
                    if(index < this.props.numberDisplay){
                        return ( 
                            <Fade key={index}>                        
                            <div className="infoCard">
                                <div className="physicalInfo">
                                    <div className="infoContainer">
                                        <div className="infoElement">
                                            <a>Name:</a> <a id="bold">{inhabitant.name}</a>
                                        </div>
                                        <div className="infoElement">
                                            <a>Age: </a> <a id="bold">{inhabitant.age}</a>
                                        </div>
                                        <div className="infoElement">
                                            <a>Hair Color: </a> <a id="bold"> {inhabitant.hair_color}</a>
                                        </div>
                                        <div className="infoElement">
                                            <a>Height: </a> <a id="bold"> {(parseFloat(inhabitant.height)).toFixed(2).toString().replace(".",",")}</a>
                                        </div>
                                        <div className="infoElement">
                                            <a>Width: </a> <a id="bold"> {(parseFloat(inhabitant.weight)).toFixed(2).toString().replace(".",",")}</a>
                                        </div>
                                    </div>
                                    <div className="imageContainer">
                                        <img src={image}/>
                                    </div>
                                </div>
                                <div className="additionalInfo">
                                    {inhabitant.friends.length>0?
                                    <div className="socialInfo">
                                        <a>Friends: </a> 
                                        <div className="socialInfoContent">
                                            {inhabitant.friends.map((friend,index) => {
                                                if(index < inhabitant.friends.length-1){
                                                    return (
                                                        <a key={index}> {friend}, </a>
                                                    )
                                                }
                                                else{
                                                    return (
                                                        <a key={index}> {friend} </a>
                                                    )
                                                }
                                                
                                            })}
                                        </div>
                                    </div>:null}
                                    {inhabitant.professions.length>0?
                                    <div className="professionalInfo">
                                        <a>Professions: </a>   
                                        <div className="professionalInfoContent">                             
                                            {inhabitant.professions.map((profession,index) => {
                                                if(index < inhabitant.professions.length-1){
                                                    return (
                                                        <a key={index}> {profession}, </a>
                                                    )
                                                }
                                                else{
                                                    return (
                                                        <a key={index}> {profession} </a>
                                                    )
                                                }
                                            })}
                                        </div>
                                    </div>:null
                                    }
                                    
                                    
                                </div>
                            </div>
                            </Fade>
                        )
                    }
                    
                })
            );
        }
        else{
            return (
                <div className="noInhabitants">
                    There are no Gnomes around! Join us to be the first!
                </div>
            );
        }
      }
    }
};

