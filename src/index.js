import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './routers/Routes'
import './styles/styles.scss';

const jsx = (
    <Routes/>
);
const indexRoot = document.getElementById('GnomeBook');

ReactDOM.render(jsx,indexRoot);